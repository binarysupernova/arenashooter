﻿using UnityEngine;

public class EnemyController : MonoBehaviour
{
  //===========================================================================
  // Variables

  // The player game object.
  public GameObject player;

  // Allows for the specification of a delay in enemy movement.
  public float movementDelay;

  // Used in MoveTowards to give the final maxDistanceDelta value.
  public float calculatedMove;

  // Determines if enemies can move or not. Currently, only used to stop
  // enemies when the game is over.
  public bool movementAllowed;

  //===========================================================================
  // Start method

  /// <summary>
  /// The start method.
  /// </summary>
  void Start () 
  {
    // Initialize variables
    movementAllowed = true;
    movementDelay = 5f;
    player = GameObject.Find("Player");
	}

  //===========================================================================
  // Update method is called once per frame

  /// <summary>
  /// The update method.
  /// </summary>
  void Update()
  {
    if (movementAllowed)
    {
      calculatedMove = movementDelay * Time.deltaTime;
      transform.LookAt(player.transform.position);
      transform.position = Vector3.MoveTowards(
        transform.position, 
        player.transform.position, 
        calculatedMove);
    }
  }

  /// <summary>
  /// The StopMoving method. When called, this will cause any enemy movement to stop.
  /// </summary>
  public void StopMoving()
  {
    movementAllowed = false;
  }
}
