﻿using UnityEngine;
using System.Collections;

public class ConsoleStateHandler : MonoBehaviour
{
  //===========================================================================
  // Enums

  // The ConsoleState enum. Defines the current state of this console.
  public enum ConsoleState
  {
    broken,
    repaired
  }

  public ConsoleState consoleState;

  //===========================================================================
  // Variables 

  // The player controller
  PlayerController playerController;

  // The game controller
  GameController gameController;

  
  public delegate void ConsoleRepaired();
  public static event ConsoleRepaired OnConsoleRepaired;

  public delegate void ConsoleBroken();
  public static event ConsoleBroken OnConsoleBroken;

  //===========================================================================
  // OnEnable / OnDisable tasks

  /// <summary>
  /// Tasks to carry out when GameController object becomes enabled and active.
  /// </summary>
  void OnEnable()
  {
    // Subscribe to OnBossDamage events
    GameController.OnBossDamage += HandleBossDamage;
  }

  /// <summary>
  /// Tasks to carry out when GameController object becomes disabled and inactive.
  /// </summary>
  void OnDisable()
  {
    // Unsubscribe to OnBossDamage events
    GameController.OnBossDamage -= HandleBossDamage;
  }

  //===========================================================================
  // Start method

  /// <summary>
  /// The start method.
  /// </summary>
  void Start()
  {
    playerController = GameObject.Find("Player").GetComponent<PlayerController>();
    gameController = GameObject.Find("GameController").GetComponent<GameController>();

    this.consoleState = new ConsoleState();
  }

  /// <summary>
  /// The HandleBossDamage method. Called when an OnBossDamage event is received.
  /// </summary>
  /// <param name="bossTarget">The target of the boss damage (player/console).</param>
  void HandleBossDamage(GameObject bossTarget)
  {
    if (this.consoleState == ConsoleState.repaired && this.gameObject == bossTarget)
    {
      Debug.Log("Console broken");
      this.consoleState = ConsoleState.broken;
      this.gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.white);

      if (OnConsoleBroken != null)
        OnConsoleBroken();
    }
  }

  /// <summary>
  /// The OnTriggerEnter method
  /// </summary>
  /// <param name="other">The collider of the other object that is entering this one.</param>
  void OnTriggerEnter(Collider other)
  {
    if (other.tag == "Player")
    {
      if (this.consoleState == ConsoleState.broken &&
            playerController.playerState == PlayerController.PlayerState.repairingState)
      {
        Debug.Log("Console repaired");
        this.consoleState = ConsoleState.repaired;
        this.gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.black);

        if (OnConsoleRepaired != null)
          OnConsoleRepaired();
      }
    }
  }
}
