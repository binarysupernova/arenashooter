﻿using UnityEngine;

public class PlayerStateController : MonoBehaviour
{  
  //===========================================================================
  // Variables 

  // The player controller
  PlayerController playerController;

  // The game controller
  GameController gameController;

  //===========================================================================
  // Start method

  /// <summary>
  /// The start method.
  /// </summary>
  void Start()
  {
    // Get the playerController and gameController objects
    playerController = GameObject.Find("Player").GetComponent<PlayerController>();
    gameController = GameObject.Find("GameController").GetComponent<GameController>();
  }

  void OnTriggerEnter(Collider other)
  {
    // This script is attached to the changer objects. When a player object enters one
    // of the changer objects, change the player state based on the type that was 
    // entered and carry out any required tasks (such as healing the player).
    if (other.tag == "Player")
    {
      if (this.gameObject.name == "ChangerShooter" &&
            playerController.playerState != PlayerController.PlayerState.shootingState)
      {
        Debug.Log("Now in shooting state");
        playerController.playerState = PlayerController.PlayerState.shootingState;
      }

      if (this.gameObject.name == "ChangerRepairer" &&
            playerController.playerState != PlayerController.PlayerState.repairingState)
      {
        Debug.Log("Now in repairing state");
        playerController.playerState = PlayerController.PlayerState.repairingState;
      }

      if (this.gameObject.name == "ChangerShielder" &&
      playerController.playerState != PlayerController.PlayerState.shieldingState)
      {
        Debug.Log("Now in shielding state");
        playerController.playerState = PlayerController.PlayerState.shieldingState;
      }

      if (this.gameObject.name == "ChangerHealer" &&
            playerController.playerState != PlayerController.PlayerState.healingState)
      {
        Debug.Log("Now in healing state");
        playerController.playerState = PlayerController.PlayerState.healingState;
        gameController.SetHealth(100, GameController.ActorType.playerActor);
      }
    }
  }
}
