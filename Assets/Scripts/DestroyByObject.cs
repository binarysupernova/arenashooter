﻿using UnityEngine;

public class DestroyByObject : MonoBehaviour 
{
  //===========================================================================
  // Variables 

  // The player controller
  PlayerController playerController;

  // The game controller
  GameController gameController;

  // The number of consoles to be repaired before the boss can be damaged
  int requiredConsoleTotal;

  //===========================================================================
  // Start method

  /// <summary>
  /// The start method.
  /// </summary>
  void Start()
  {
    gameController = GameObject.Find("GameController").GetComponent<GameController>();
    playerController = GameObject.Find("Player").GetComponent<PlayerController>();

    // TODO: This should probably be in the GameController, move it later.
    requiredConsoleTotal = 4;
  }

  /// <summary>
  /// The OnTriggerEnter method
  /// </summary>
  /// <param name="other">The collider of the other object that is entering this one.</param>
  void OnTriggerEnter(Collider other)
  {
    // Always destroy a laser object that collides with anything but the player or the
    // changers (as they are intended to be passable tiles).
    if (this.gameObject.tag == "Laser" &&
      other.tag != "Player" && 
      other.tag != "Changers")
    {
      Destroy(this.gameObject);
    }

    if (other.tag == "EnemyOne")
    {
      // Destroying an enemy adds 10 to score, whether destroyed by laser or 
      // collision with player.
      gameController.AddScore(10);
      Destroy(other.gameObject);

      if (this.gameObject.tag == "Player")
      {
        // If the player is in shielding state, subtract 1 health. Else, subtract 10.
        gameController.SubtractHealth(
          playerController.playerState == 
            PlayerController.PlayerState.shieldingState ? 1 : 10, 
            GameController.ActorType.playerActor);
        return;
      }
    }

    if (other.tag == "Sentry")
    {
      // Take 1 damage off boss if it's in damageable state (all consoles are repaired)
      if (gameController.numConsolesRepaired == requiredConsoleTotal)
      {
        gameController.SubtractHealth(1, GameController.ActorType.bossActor);
      }
    }
  }
}
