﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour 
{
  //===========================================================================
  // Actor type enum

  // Allows for specification of the actor type. More can be added as necessary.
  public enum ActorType
  {
    // The player actor
    playerActor,

    // The boss actor
    bossActor
  }

  //===========================================================================
  // Enemy variables

  // TODO: TEMP - number of enemies. This should change later with more levels.
  public int numberOfEnemies;

  // TODO: TEMP - enemy game object. This should change later with more enemies.
  public GameObject enemyObject;

  // TODO: Spawn location for enemies. This should change later with more levels. 
  private float horizontalSpawnLoc;
  private float verticalSpawnLoc;

  // TODO: List of playfield boundary points. Used to spawn enemies in corners.
  // Will probably change later with more levels/enemies.
  List<float> horzBoundaryArray;
  List<float> vertBoundaryArray;

  // Safe zone around player - enemies cannot spawn within x value of the player.
  // TODO: See if this still needed later on.
  private int playerSafeZone = 10;

  //===========================================================================
  // Boss variables

  // Delay between shots before next boss shot can be fired. The current value of
  // shotTimer is compared to shotDelay to determine if a shot can be taken.
  public float bossShotDelayMin;
  public float bossShotDelayMax;
  private float bossShotTimer;

  // Holds the boss laser shot prefab.
  public GameObject bossLaserShot;

  // Holds the boss sentry turret object.
  public GameObject bossSentryTurret;

  // Holds a newly instantiated boss laser shot.
  private GameObject newBossLaserShot;

  // Holds a line renderer component for a new laser shot.
  private LineRenderer bossLaserRenderer;

  // Boss damage event handler
  public delegate void BossDamage(GameObject bossTarget);
  public static event BossDamage OnBossDamage;

  // Tracks the number of consoles repaired. If all on the level are repaired,
  // we can damage the boss in shooting state.
  public int numConsolesRepaired;

  //===========================================================================
  // UI variables (Score, health, etc.)

  // Score object & text for tracking player score
  private Text scoreObject;
  private int scoreTotal;

  // Health object & value for tracking player health
  private Text playerHealthObject;
  private int playerHealthTotal;

  // Health object & value for tracking boss health
  private Text bossHealthObject;
  private int bossHealthTotal;

  // Game info text object. Gives information to the player (game over, etc.)
  private Text gameInfoTextObject;

  //===========================================================================
  // Player variables

  // The player game object. Used to destroy player when GameOver() is called.
  // TODO: Look at this later and see if its declaration/use needs revising.
  public GameObject player;

  //===========================================================================
  // OnEnable / OnDisable tasks

  /// <summary>
  /// Tasks to carry out when GameController object becomes enabled and active.
  /// </summary>
  void OnEnable()
  {
    // Subscribe to OnConsoleRepaired events
    ConsoleStateHandler.OnConsoleRepaired += IncrementRepairedConsoles;
    ConsoleStateHandler.OnConsoleBroken += DecrementRepairedConsoles;
  }

  /// <summary>
  /// Tasks to carry out when GameController object becomes disabled and inactive.
  /// </summary>
  void OnDisable()
  {
    // Unsubscribe to OnConsoleRepaired events
    ConsoleStateHandler.OnConsoleRepaired -= IncrementRepairedConsoles;
    ConsoleStateHandler.OnConsoleBroken -= DecrementRepairedConsoles;
  }

  //===========================================================================
  // Start method

  /// <summary>
  /// The start method.
  /// </summary>
  void Start ()
  {
    bossSentryTurret = GameObject.Find("SentryTurret");

    player = GameObject.Find("Player");
    numberOfEnemies = 100;
    numConsolesRepaired = 0;

    scoreObject = GameObject.Find("ScoreText").GetComponent<Text>();
    scoreObject.text = "Score: 0";
    playerHealthObject = GameObject.Find("PlayerHealthText").GetComponent<Text>();
    bossHealthObject = GameObject.Find("BossHealthText").GetComponent<Text>();

    gameInfoTextObject = GameObject.Find("GameInfoText").GetComponent<Text>();
    gameInfoTextObject.text = string.Empty;

    SetHealth(100, ActorType.playerActor);
    SetHealth(200, ActorType.bossActor);

    horzBoundaryArray = new List<float> { BoundaryHandler.xMin, BoundaryHandler.xMax };
    vertBoundaryArray = new List<float> { BoundaryHandler.zMin, BoundaryHandler.zMax };

    bossShotDelayMin = 1.0f;
    bossShotDelayMax = 10.0f;
    bossShotTimer = Time.time + Random.Range(bossShotDelayMin, bossShotDelayMax);

    // Call SpawnEnemies() method on a time delay, and keep doing it.
    InvokeRepeating("SpawnEnemies", Time.deltaTime + 0.5f, 0.5f);
  }

  //===========================================================================
  // Update method is called once per frame

  /// <summary>
  /// The update method.
  /// </summary>
	void Update ()
  {
    if (playerHealthTotal <= 0)
    {
      GameOver();
    }

    if (bossHealthTotal <= 0)
    {
      NextLevel();
    }
	}

  //===========================================================================
  // Fixedupdate is called every fixed framerate frame
  void FixedUpdate()
  {
    // Check if current time is greater than last set shot timer. If it is, we
    // can shoot a new laser. Player also needs to be in shooting state, and if
    // either the player or boss are dead, don't shoot any more.
    if (Time.time > bossShotTimer && 
      playerHealthTotal > 0 && 
      bossHealthTotal > 0)
    {
     bossShotTimer = Time.time + Random.Range(bossShotDelayMin, bossShotDelayMax);
     InvokeBossDamage();
    }
  }

  //===========================================================================
  // Game-related helper methods - General

  /// <summary>
  /// Adds the passed-in value to the running score total and updates the
  /// score GUI object with the new total.
  /// </summary>
  /// <param name="value">The value to add to the running total</param>
  public void AddScore(int value)
  {
    scoreTotal = scoreTotal + value;
    scoreObject.text = "Score: " + scoreTotal;
  }

  /// <summary>
  /// Adds the passed-in value to the health of the specified actor.
  /// </summary>
  /// <param name="value">The value to add to the specified actor's total.</param>
  /// <param name="actor">The actor which will have health added to them.</param>
  public void AddHealth(int value, ActorType actor)
  {
    if (actor == ActorType.playerActor)
    {
      playerHealthTotal = playerHealthTotal + value;
      playerHealthObject.text = "Health: " + playerHealthTotal;
    }

    if (actor == ActorType.bossActor)
    {
      bossHealthTotal = bossHealthTotal + value;
      bossHealthObject.text = "Boss: " + bossHealthTotal;
    }
  }

  /// <summary>
  /// Subtracts the passed-in value from the health of the player.
  /// </summary>
  /// <param name="value">The value to subtract from the specified actor's total.</param>
  /// <param name="actor">The actor which will have health subtracted from them.</param>
  public void SubtractHealth(int value, ActorType actor)
  {
    if (actor == ActorType.playerActor)
    {
      playerHealthTotal = playerHealthTotal - value;
      playerHealthObject.text = "Health: " + playerHealthTotal;
    }

    if (actor == ActorType.bossActor)
    {
      bossHealthTotal = bossHealthTotal - value;
      bossHealthObject.text = "Boss: " + bossHealthTotal;
    }
  }

  /// <summary>
  /// Sets the passed-in value to the health of the player.
  /// </summary>
  /// <param name="value">The value to set the actor's health total to.</param>
  /// <param name="actor">The actor which will have their health set to the value.</param>
  public void SetHealth(int value, ActorType actor)
  {
    if (actor == ActorType.playerActor)
    {
      playerHealthTotal = value;
      playerHealthObject.text = "Health: " + playerHealthTotal;
    }

    if (actor == ActorType.bossActor)
    {
      bossHealthTotal = value;
      bossHealthObject.text = "Boss: " + bossHealthTotal;
    }
  }

  /// <summary>
  /// Increments the number of consoles repaired when an OnConsoleRepaired
  /// event is received.
  /// </summary>
  public void IncrementRepairedConsoles()
  {
    numConsolesRepaired++;
  }

  /// <summary>
  /// Increments the number of consoles repaired when an OnConsoleBroken
  /// event is received.
  /// </summary>
  public void DecrementRepairedConsoles()
  {
    numConsolesRepaired--;
  }

  //===========================================================================
  // Game-related helper methods - Enemy/Boss handling

  /// <summary>
  /// Handles the spawning of enemies
  /// </summary>
  public void SpawnEnemies()
  {
    // Spawn randomly from the corners
    horizontalSpawnLoc = horzBoundaryArray[Random.Range(0, horzBoundaryArray.Count)];
    verticalSpawnLoc = vertBoundaryArray[Random.Range(0, vertBoundaryArray.Count)];

    Instantiate(
      enemyObject,
      new Vector3(
        horizontalSpawnLoc, 
        0,
        verticalSpawnLoc), 
      transform.rotation);
  }

  /// <summary>
  /// Handles the spawning of enemies
  /// </summary>
  public void InvokeBossDamage()
  {
    var bossTarget = PickBossTarget();

    if (bossTarget == player.gameObject)
    {
      SubtractHealth(5, ActorType.playerActor);
    }

    else
    {
      OnBossDamage(bossTarget);
    }

    DrawBossLaser(bossTarget);
  }

  public GameObject PickBossTarget()
  {
    var pickResult = Random.Range(0, 2);

    if (pickResult == 0)
    {
      return player.gameObject;
    }

    else
    {
      GameObject[] consoleObjectList;

      consoleObjectList = GameObject.FindGameObjectsWithTag("Consoles");

      GameObject returnObject = consoleObjectList[Random.Range(0, consoleObjectList.Length)];

      return returnObject;
    }
  }

  public void DrawBossLaser(GameObject bossTarget)
  {
    newBossLaserShot = Instantiate(bossLaserShot);
    bossLaserRenderer = newBossLaserShot.GetComponent<LineRenderer>();
    var bossLaserCollider = newBossLaserShot.GetComponent<BoxCollider>();

    bossLaserRenderer.SetPosition(0, bossSentryTurret.transform.position);
    bossLaserRenderer.SetPosition(1, bossTarget.transform.position);

    Destroy(newBossLaserShot, 0.1f);
  }

  //===========================================================================
  // Next level handling

  /// <summary>
  /// The NextLevel method. Advances the game to the next level.
  /// TODO: This is all totally placeholder until more levels are implemented.
  /// </summary>
  public void NextLevel()
  {
    var enemyObjects = GameObject.FindGameObjectsWithTag("EnemyOne");
    foreach (GameObject enemy in enemyObjects)
    {
      Destroy(enemy);
    }

    var sentryObjects = GameObject.FindGameObjectsWithTag("Sentry");
    foreach (GameObject sentry in sentryObjects)
    {
      Destroy(sentry);
    };

    StartCoroutine(ReplaceThisMethod());
  }

  /// <summary>
  /// // TODO: This needs to be replaced - it's just a temp way to restart the level
  /// </summary>
  IEnumerator ReplaceThisMethod()
  {
    gameInfoTextObject.color = Color.green;
    gameInfoTextObject.text = "You killed the Sentry!\nMoving to next level...";

    yield return new WaitForSeconds(5);
    Application.LoadLevel(Application.loadedLevel);
  }


  //===========================================================================
  // Endgame handling

  /// <summary>
  /// Called when the game is over (player health is 0 or less)
  /// </summary>
  public void GameOver()
  {
    playerHealthObject.text = "GAME OVER";

    gameInfoTextObject.color = Color.red;
    gameInfoTextObject.text = "GAME OVER";

    Destroy(player);
    var enemyObjects = GameObject.FindGameObjectsWithTag("EnemyOne");
    foreach (GameObject enemy in enemyObjects)
    {
      enemy.GetComponent<EnemyController>().StopMoving();
    }

    StartCoroutine(ReplaceThisOtherMethod());
  }

  /// <summary>
  /// // TODO: This needs to be replaced - it's just a temp way to restart the level
  /// </summary>
  IEnumerator ReplaceThisOtherMethod()
  {
    print("Restarting Game");
    yield return new WaitForSeconds(5);
    Application.LoadLevel(Application.loadedLevel);
  }
}
