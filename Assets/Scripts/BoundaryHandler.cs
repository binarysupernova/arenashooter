﻿using UnityEngine;

public class BoundaryHandler : MonoBehaviour
{
  //===========================================================================
  // Variables 

  // The max and min boundary values for X and Z.
  public static float xMin;
  public static float xMax;
  public static float zMin;
  public static float zMax;

  //===========================================================================
  // Start method

  /// <summary>
  /// The start method.
  /// </summary>
	void Start () 
  {
    // Calculate and assign boundary values
    Vector3 topRight = new Vector3(28.5f, 0f, 28f);
    Vector3 lowerLeft = new Vector3(-28.5f, 0f, -10.5f);

    xMin = lowerLeft.x;
    xMax = topRight.x;
    zMin = lowerLeft.z;
    zMax = topRight.z;
	}
}
