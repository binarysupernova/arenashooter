﻿using UnityEngine;

public class ShotHandler : MonoBehaviour
{
  //===========================================================================
  // Variables 

  // The speed of the laser
  public float speed;

  // The player controller
  public PlayerController playerController;

  // The life of instantiated laser bullets. This ensures that we clean-up
  // any bullets that aren't destroyed through contact.
  private int laserLife = 5;

  //===========================================================================
  // Start method

  /// <summary>
  /// The start method.
  /// </summary>
  void Start () 
  {
    // Get the PlayerController component from the Player object. From this, we
    // obtain the shot direction from the player's input and fire a laser in that
    // direction.
    playerController = GameObject.Find("Player").GetComponent<PlayerController>();

    GetComponent<Rigidbody>().AddForce(
      playerController.shotMove.normalized * speed * Time.deltaTime);

    // If the bullet doesn't hit anything within bulletLife duration, destroy it.
    Destroy(this.gameObject, laserLife);
	}
}
