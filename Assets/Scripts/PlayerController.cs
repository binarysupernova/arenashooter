﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
  //===========================================================================
  // Enums

  // The PlayerState enum. Defines the current state of the player
  public enum PlayerState {
 
    // In shootingState, can shoot enemies, & sentry when all consoles repaired
    shootingState,

    // In repairState, consoles can be repaired
    repairingState,

    // In shieldState, player ONLY takes damage from random sentry fire
    shieldingState,

    // In healState, player's health set to full on change, no other benefit
    healingState
  }

  public PlayerState playerState;

  //===========================================================================
  // Movement variables

  // Player speed
  public float speed = 20f;

  // Boundary offsets for movement. Allows for tweaking of playfield size that
  // the player is allowed to move in.
  private const float boundaryOffsetX = 0f;
  private const float boundaryOffsetZ = 0f;

  // Stores horizontal and vertical values captured from input devices for 
  // player movement (left analog stick, or W, A, S, D).
  public Vector3 playerMove;

  //===========================================================================
  // Laser (shooting) variables

  // Holds the Laser prefab.
  public GameObject laserShot;

  // The shotOrigin GameObject is located at the center of the Player object, and
  // is a child of the Player GameObject.
  public Transform shotOrigin;

  // Delay between shots before next shot can be fired. The current value of
  // shotTimer is compared to shotDelay to determine if a shot can be taken.
  public float shotDelay;
  private float shotTimer;

  // Stores horizontal and vertical values captured from input devices for 
  // player shooting (right analog stick, or I, J, K, L).
  public Vector3 shotMove;

  // Specified deadzone for shooting. This helps to avoid unintended shooting due
  // to small input values on the right analog stick.
  float deadzone = 0.1f;

  //===========================================================================
  // Start method

  /// <summary>
  /// The start method.
  /// </summary>
  void Start()
  {
    // Initialize player with default (shooting) state
    playerState = PlayerState.shootingState;
  }

  //===========================================================================
  // Update method is called once per frame

  /// <summary>
  /// The update method.
  /// </summary>
	void Update () 
  {
    // Get horizontal and vertical input from the left stick or keyset.
    playerMove = new Vector3(
      Input.GetAxisRaw("Horizontal"), 
      0.0f,
      Input.GetAxisRaw("Vertical"));

    // Get horizontal and vertical input from the right stick or keyset.
    shotMove = new Vector3(
      Mathf.Round(Input.GetAxisRaw("RightHorizontal")),
      0.0f,
      Mathf.Round(Input.GetAxisRaw("RightVertical")));

    // Ignore input below the specified deadzone value
    if (Mathf.Abs(shotMove.x) < deadzone)
    {
      shotMove.x = 0.0f;
    }

    if (Mathf.Abs(shotMove.z) < deadzone)
    {
      shotMove.z = 0.0f;
    }
	}

  //===========================================================================
  // Fixedupdate is called every fixed framerate frame
  void FixedUpdate ()
  {
    // Move player using normalized values to give consistent speed in diagonals.
    transform.Translate(playerMove.normalized * speed * Time.deltaTime);

    // Setup movement boundaries based on current position.
    Vector3 newPos = new Vector3
    (
        Mathf.Clamp(
          GetComponent<Rigidbody>().position.x, 
          BoundaryHandler.xMin + boundaryOffsetX,
          BoundaryHandler.xMax - boundaryOffsetX),
        0.0f,
        Mathf.Clamp(
          GetComponent<Rigidbody>().position.z,
          BoundaryHandler.zMin + boundaryOffsetZ,
          BoundaryHandler.zMax - boundaryOffsetZ)
    );

    // Update position of player GameObject to respect boundaries.
    transform.position = newPos;

    // Check if current time is greater than last set shot timer. If it is, we
    // can shoot a new laser. Player also needs to be in shooting state.
    if (Time.time > shotTimer
      && (shotMove.x != 0.0f || shotMove.z != 0.0f)
      && playerState == PlayerState.shootingState)
    {
      shotTimer = Time.time + shotDelay;
      ShootLaser();
    }
  }

  /// <summary>
  /// The ShootLaser method. Instantiates a laserShot GameObject prefab at the
  /// specified position and rotation. Movement of the laser is handled by the
  /// ShotHandler class.
  /// </summary>
  void ShootLaser()
  {
    Instantiate(laserShot, shotOrigin.position, shotOrigin.rotation);
  }
}
