# ArenaShooter game (real name TBC) #
### Instructions (Draft): ###
Escape the Complex of Eternal Evil by destroying the room sentries on each step of your journey!

**Scoring:**

* Minions = 10 points.
* Room Sentry = 5000 points.

**Gameplay Elements:**

*Changers* - allow you to change role:

* Shooting - allows you to shoot minions and room sentries (when their shields are down).
* Repairing - allows you to repair broken consoles.
* Healing - immediately replenishes life back to full.
* Shielding - any minion damage taken is reduced to 1.

*Consoles* - repair all consoles in order to lower the room sentry's shield! Once lowered, damage can be inflicted while in the Shooting role.

*Minions* - footmen of the Complex of Eternal Evil, who cause heavy damage on contact (considerably less in shielding state).

*Sentry* - kill the sentry to move to the next room! Beware - the sentry will periodically shoot you or knock out active consoles!

**Controls:**

Currently, XBox 360 controllers are supported (XBOne currently not tested).

Keyboard is also supported - WASD to move, IJKL to shoot.

### TODO List: ###

Gameplay:

* Implement additional levels (including level layouts).
* Implement additional enemy types.
* Implement non-placeholder graphics (models, animations, environments, effects, explosions etc.)
* Implement sound & music.
* Improve UI (health bars, state indicator, etc.)
* Implement title screen.
* Changers should not be available all the time - put them on a timer. (NOTE: Shielding will be a LOT more desirable when the changers are on timers)
* Review and balance level difficulty and gameplay timers. It should not be impossible or overly frustrating to repair all the consoles and kill the boss. Difficulty should also ramp up smoothly.
* Review spawning behavior of minions - explore wave-style spawning, and consider different spawn locations.
* Score currently doesn't carry over between levels - needs to be fixed.

Code/engine improvements:

* Explore expanding use of events to encompass other areas which currently interact directly with external classes (for example, in PlayerStateController.cs).
* Improve collision for gameplay objects (ie, stop rigidbody 'bouncing').
* Review location of requiredConsoleTotal - it should probably be in GameController.cs instead of DestroyByObject.
* Add additional documentation where needed.